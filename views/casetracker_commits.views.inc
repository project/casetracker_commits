<?php

function casetracker_commits_views_data() {
  // CASETRACKER COMMITS

  $data['casetracker_commits']['table']['group']  = t('Casetracker commits');

  $data['casetracker_commits']['table']['base'] = array(
    'field' => 'ctcid',
    'title' => t('Case Tracker Commits'),
    'help' => t("Case Tracker Commits ID."),
  );

  $data['casetracker_commits']['table']['join'] = array(
    'versioncontrol_operations' => array(
      'left_field' => 'vc_op_id',
      'field' => 'vc_op_id',
    ),
    'node' => array(
      'left_field' => 'nid',
      'field' => 'nid',
    ),
  );

  $data['casetracker_commits']['nid'] = array(
    'title' => t('Case Node'),
    'help' => t('The Case Node the commit belongs to'),
    'relationship' => array(
      'base' => 'node',
      'field' => 'nid',
      'handler' => 'views_handler_relationship',
      'label' => t('Node'),
    ),
  );

  $data['casetracker_commits']['vc_op_id'] = array(
    'title' => t('Version control operation'),
    'help' => t('The version control operation relationship'),
    'relationship' => array(
      'base' => 'versioncontrol_operations',
      'field' => 'vc_op_id',
      'handler' => 'views_handler_relationship',
      'label' => t('Commit operation'),
    ),
  );

  return $data;
}
