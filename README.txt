Case Tracker Commits integrate Version Control API with Case Tracker
intercepting the commit messages parsed by Version Control and matching some
patterns that identify a relation with a case ID.

The current pattern we use is: ref [case_nid] text_message

Provides some blocks that could be added in the Case Tracker Nodes that list
all the commits of the case. This is handled via Views and could be configured
all the fields that you want to show in that block.

How it works
------------

Version Control API Git integrate repositories with Drupal in the configuration
of VCS Repositories. This configuration tells Drupal to track all the changes
that happen in that repositories this task is delegated to Drush command
"vcapi-parse-logs" this task get all the logs executing the command git log via
git binary and retrieves all the commit information that is stored in database
table: (versioncontrol_operations)

Note: the Git repositories must be in the same server where the Drupal instance that will execute "vcapi-parse-logs".

In the process of parse logs each entry call a
hook_versioncontrol_entity_commit_insert passing the operation information.
Case Tracker Commits implement that hook and use it to parse the message to
search a case_nid pattern that permit relate operation with case.

Dependencies 
------------

I haved a chance to talk a little bit with one of maintainers of Version
Control API @marvil07 and explain me about the total rewrite of this module
that was inherited from other developers. No stable release was launched at
this moment but they are using the dev version integrated in Drupal.org

For the above reasons don't try to install the default versions you must use:

- casetracker stable release
- versioncontrol-6.x-2.x-dev 
- versioncontrol_git-6.x-2.x-dev 

Installation
------------

I recommend install with Drush (http://drupal.org/project/drush)
drush dl casetracker
drush dl versioncontrol-6.x-2.x-dev
drush dl versioncontrol_git-6.x-2.x-dev
drush dl autoload
drush dl dbtng
drush dl views_field_view

Enable the modules with dependencies:

Version Control API
Git backend
Commit Log

Configuration
-------------

- Go to Version Control settings (admin/settings/versioncontrol-settings) and
  configure at Git Backend configuration the Git binary path that must point to
  the git executable. In mac I use fink to install git client my path is
  /sw/bin/git

  In plugins tab you can configure the mapping users plugin. At default mapping
  plugin set "Map using Drupal user email".

- Go to VCS Repositories configuration
  (admin/content/versioncontrol-repositories/add-git) and Add Git Repository:

  In the repository root you must enter the path to you master repository in
  case you use a clone of that repository point the path to the hidden .git
  directory.  For example: /Users/pablocc/Sites/Ideup/fiat-concesionarios/.git

  Set default Update Method "Automatic log retrieval at cron". I tried the
  external script that get the commit info via git hook, but currently doesn't
  work. If active the Author and Committer mappings the log retrieval task will
  try to map the git and drupal user by the email to store an uid for each
  operation.

- Execute drush vcapi-parse-logs and if successful executed add this command to
  your crontab to ensure periodically sync. I recommend add execution each 3
  minutes.

