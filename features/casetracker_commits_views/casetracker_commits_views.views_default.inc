<?php

/**
 * Implementation of hook_views_default_views().
 */
function casetracker_commits_views_views_default_views() {
  $views = array();

  // Exported view: casetracker_commits
  $view = new view;
  $view->name = 'casetracker_commits';
  $view->description = 'Case tracker commits reports.';
  $view->tag = 'casetracker';
  $view->view_php = '';
  $view->base_table = 'casetracker_commits';
  $view->is_cacheable = FALSE;
  $view->api_version = 2;
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */
  $handler = $view->new_display('default', 'Defaults', 'default');
  $handler->override_option('relationships', array(
    'nid' => array(
      'label' => 'Case Node',
      'required' => 1,
      'id' => 'nid',
      'table' => 'casetracker_commits',
      'field' => 'nid',
      'relationship' => 'none',
    ),
    'vc_op_id' => array(
      'label' => 'Commit operation',
      'required' => 0,
      'id' => 'vc_op_id',
      'table' => 'casetracker_commits',
      'field' => 'vc_op_id',
      'relationship' => 'none',
    ),
  ));
  $handler->override_option('fields', array(
    'date' => array(
      'label' => 'Date',
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 0,
        'path' => '',
        'link_class' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'target' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'html' => 0,
        'strip_tags' => 0,
      ),
      'empty' => '',
      'hide_empty' => 0,
      'empty_zero' => 0,
      'date_format' => 'large',
      'custom_date_format' => '',
      'link' => 1,
      'exclude' => 0,
      'id' => 'date',
      'table' => 'versioncontrol_operations',
      'field' => 'date',
      'relationship' => 'vc_op_id',
    ),
    'attribution' => array(
      'label' => '',
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 0,
        'path' => '',
        'link_class' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'target' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'html' => 0,
        'strip_tags' => 0,
      ),
      'empty' => '',
      'hide_empty' => 0,
      'empty_zero' => 0,
      'exclude' => 0,
      'id' => 'attribution',
      'table' => 'versioncontrol_operations',
      'field' => 'attribution',
      'relationship' => 'vc_op_id',
    ),
    'revision' => array(
      'label' => 'Revision',
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 0,
        'path' => '',
        'link_class' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'target' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'html' => 0,
        'strip_tags' => 0,
      ),
      'empty' => '',
      'hide_empty' => 0,
      'empty_zero' => 0,
      'exclude' => 0,
      'id' => 'revision',
      'table' => 'versioncontrol_operations',
      'field' => 'revision',
      'relationship' => 'vc_op_id',
    ),
    'message' => array(
      'label' => 'Message',
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 0,
        'path' => '',
        'link_class' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'target' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'html' => 0,
        'strip_tags' => 0,
      ),
      'empty' => '',
      'hide_empty' => 0,
      'empty_zero' => 0,
      'issue_tracker_url' => '',
      'exclude' => 0,
      'id' => 'message',
      'table' => 'versioncontrol_operations',
      'field' => 'message',
      'relationship' => 'vc_op_id',
    ),
  ));
  $handler->override_option('sorts', array(
    'vc_op_id' => array(
      'order' => 'DESC',
      'id' => 'vc_op_id',
      'table' => 'versioncontrol_operations',
      'field' => 'vc_op_id',
      'relationship' => 'vc_op_id',
    ),
  ));
  $handler->override_option('arguments', array(
    'nid' => array(
      'default_action' => 'default',
      'style_plugin' => 'default_summary',
      'style_options' => array(),
      'wildcard' => 'all',
      'wildcard_substitution' => 'All',
      'title' => 'Case nid',
      'breadcrumb' => '',
      'default_argument_type' => 'node',
      'default_argument' => '',
      'validate_type' => 'none',
      'validate_fail' => 'empty',
      'break_phrase' => 0,
      'not' => 0,
      'id' => 'nid',
      'table' => 'node',
      'field' => 'nid',
      'relationship' => 'nid',
      'validate_user_argument_type' => 'uid',
      'validate_user_roles' => array(
        '2' => 0,
        '3' => 0,
        '6' => 0,
        '5' => 0,
        '4' => 0,
      ),
      'default_options_div_prefix' => '',
      'default_argument_fixed' => '',
      'default_argument_user' => 0,
      'default_argument_php' => '',
      'validate_argument_node_type' => array(
        'blog' => 0,
        'book' => 0,
        'event' => 0,
        'feed_ical' => 0,
        'feed_ical_item' => 0,
        'group' => 0,
        'profile' => 0,
        'shoutbox' => 0,
        'casetracker_basic_case' => 0,
        'casetracker_basic_project' => 0,
        'feed' => 0,
      ),
      'validate_argument_node_access' => 0,
      'validate_argument_nid_type' => 'nid',
      'validate_argument_vocabulary' => array(
        '1' => 0,
      ),
      'validate_argument_type' => 'tid',
      'validate_argument_transform' => 0,
      'validate_user_restrict_roles' => 0,
      'validate_argument_is_member' => 'OG_VIEWS_DO_NOT_VALIDATE_MEMBERSHIP',
      'validate_argument_group_node_type' => array(
        'group' => 0,
      ),
      'validate_argument_php' => '',
    ),
  ));
  $handler->override_option('filters', array(
    'vcs' => array(
      'operator' => 'in',
      'value' => array(
        'git' => 'git',
      ),
      'group' => '0',
      'exposed' => FALSE,
      'expose' => array(
        'operator' => FALSE,
        'label' => '',
      ),
      'id' => 'vcs',
      'table' => 'versioncontrol_repositories',
      'field' => 'vcs',
      'relationship' => 'vc_op_id',
    ),
  ));
  $handler->override_option('access', array(
    'type' => 'perm',
    'perm' => 'access content',
  ));
  $handler->override_option('cache', array(
    'type' => 'none',
  ));
  $handler->override_option('title', 'Case Commit Blocks');
  $handler->override_option('items_per_page', 20);
  $handler->override_option('use_more', 1);
  $handler->override_option('use_more_always', 0);
  $handler->override_option('use_more_text', 'more commits');
  $handler = $view->new_display('block', 'Case Commits Block', 'block_1');
  $handler->override_option('style_plugin', 'list');
  $handler->override_option('block_description', '');
  $handler->override_option('block_caching', -1);
  $translatables['casetracker_commits'] = array(
    t('Case Commit Blocks'),
    t('Case Commits Block'),
    t('Defaults'),
    t('more commits'),
  );

  $views[$view->name] = $view;

  return $views;
}
